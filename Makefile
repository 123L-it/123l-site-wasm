export GO111MODULE=on

# Setup install tools
toolsGo := tools.go
toolsDir := bin/
toolPkgs := $(shell go list -f '{{join .Imports " "}}' ${toolsGo})
toolCmds := $(foreach tool,$(notdir ${toolPkgs}),${toolsDir}/${tool})
$(foreach cmd,${toolCmds},$(eval $(notdir ${cmd})Cmd := ${cmd}))

# Setup extend env variables and default values
-include .env
export

GOBIN = $(shell realpath "./bin")
GOROOT= $(shell go env GOROOT)

# Declare make targets
go.mod: ${toolsGo}
	go mod tidy
	touch go.mod

${toolCmds}: go.mod
	go build -o $@ $(filter %/$(@F),${toolPkgs})

tools: ${toolCmds}

build.wasm:
	cp --remove-destination "$(GOROOT)/misc/wasm/wasm_exec.js" ./assets/
	GOOS=js GOARCH=wasm go build -o assets/app.wasm cmd/wasm/main.go

build: build.wasm
	go build -o ./dist/

run: build
	go run main.go

test:
	go test ./...

fmt:
	go fmt ./...

fmt-check:
	gofmt -e -d *.go

.PHONY: tools build fmt fmt-check
