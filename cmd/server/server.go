package server

import (
	"fmt"
	"net/http"
)

func Start() {
	webdir := http.Dir("assets")
	err := http.ListenAndServe("localhost:6161", http.FileServer(webdir))

	if err != nil {
		fmt.Println("Failed to start server", err)
		return
	}
}
