package main

import (
	"fmt"
	"github.com/realPy/hogosuru"
	"github.com/realPy/hogosuru/document"
	"github.com/realPy/hogosuru/htmlheadingelement"
)

func main() {
	c := make(chan bool)
	hogosuru.Init()

	fmt.Println("This is a webassembly side for 123L")

	doc, err := document.New()
	hogosuru.AssertErr(err)

	body, err := doc.Body()
	hogosuru.AssertErr(err)

	h1, err := htmlheadingelement.NewH1(doc)
	hogosuru.AssertErr(err)

	h1.SetTextContent("This is a webassembly site for 123L")
	body.AppendChild(h1.Node)

	<-c
}
