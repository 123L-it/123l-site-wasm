# 123L site

123L website implementation in WebAssembly using GoLang.

## Requirements

- Go 1.20.2 or higher
- Make

## Usage

```sh
make run
```

then open `localhost:6161` in your browser.

## Commands

- Build the wasm app and the server
```sh
make build
```

- To run the tests
```sh
make test
```

- To verify the correct format
```sh
make fmt
```

- If you want install tools
```sh
make tools
```
