package counter

import "testing"

func TestCounter(t *testing.T) {
	initial_expected := 1
	if got := Increment(); got != initial_expected {
		t.Errorf("Expected %d, got: %d", initial_expected, got)
	}
	
	increment_expected := 2
	if got :=  Increment();  got != increment_expected {
		t.Errorf("Expected %d, got: %d", increment_expected, got)
	}

	decrement_expected := 1
	if got := Decrement(); got != decrement_expected {
		t.Errorf("Expected %d, got: %d", decrement_expected, got)
	}
}
