package counter

var count int

func Increment() int {
	count++
	return count
}

func Decrement() int {
	count--
	return count
}
